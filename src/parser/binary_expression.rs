use crate::{
  ast::{BinaryExpression, BinaryOperator, ErrorToken, Expression},
  parser::{
    simple_expr,
    utils::{keyword, IResult, LocatedSpan},
  },
};
use nom::{
  branch::alt,
  bytes::complete::tag,
  combinator::value,
  error::{FromExternalError, ParseError},
  multi::many1,
  sequence::tuple,
};
use std::{iter::Peekable, vec::IntoIter};

#[derive(Debug, PartialEq, Clone)]
enum Token {
  Operator(BinaryOperator),
  Expression(Expression),
}
impl Token {
  pub fn precedence(&self) -> u8 {
    match self {
      Token::Operator(op) => op.precedence(),
      _ => u8::max_value(),
    }
  }
}
#[derive(Debug)]
enum AstError {
  ExpectedOperator,
  ExpectedToken,
}

impl<'a> ParseError<LocatedSpan<'a>> for AstError {
  fn from_error_kind(_: LocatedSpan, _: nom::error::ErrorKind) -> Self {
    todo!()
  }
  fn append(_: LocatedSpan, _: nom::error::ErrorKind, _: Self) -> Self {
    todo!()
  }
}

struct PrecedenceSolver {
  token_iter: Peekable<IntoIter<Token>>,
}

impl PrecedenceSolver {
  fn nud(&mut self, t: Token) -> Expression {
    match t {
      Token::Expression(e) => e,
      _ => Expression::Error(ErrorToken::Unexpected),
    }
  }

  fn led(&mut self, bp: u8, left: Expression, op: Token) -> Result<Expression, AstError> {
    match op {
      Token::Operator(operator) => {
        let right = self.expr(bp)?;

        Ok(Expression::BinaryExpression(Box::new(BinaryExpression {
          lhs: left,
          rhs: right,
          op: operator,
        })))
      }
      _ => Err(AstError::ExpectedOperator),
    }
  }

  fn expr(&mut self, rbp: u8) -> Result<Expression, AstError> {
    let first_token = self.token_iter.next().ok_or(AstError::ExpectedToken)?;
    let mut left = self.nud(first_token);

    while let Some(peeked) = self.token_iter.peek() {
      if rbp >= peeked.precedence() {
        break;
      }

      let op = self.token_iter.next().unwrap();
      left = self.led(op.precedence(), left, op)?;
    }

    Ok(left)
  }

  fn solve(tokens: IntoIter<Token>) -> Result<Expression, AstError> {
    let mut solver = PrecedenceSolver {
      token_iter: tokens.peekable(),
    };
    solver.expr(0)
  }
}

pub(crate) fn binary_expr(i: LocatedSpan) -> IResult<Expression> {
  let (i, first_expr) = simple_expr(i)?;
  let first_token = Token::Expression(first_expr);
  let (i, other_tokens) = many1(tuple((binary_operator, simple_expr)))(i)?;
  let mut tokens = vec![first_token];
  for (sep, expr) in other_tokens {
    tokens.push(Token::Operator(sep));
    tokens.push(Token::Expression(expr));
  }

  match PrecedenceSolver::solve(tokens.into_iter()) {
    Ok(expr) => Ok((i, expr)),
    Err(err) => Err(nom::Err::Error(nom::error::Error::from_external_error(
      i,
      nom::error::ErrorKind::MapRes,
      err,
    ))),
  }
}

fn binary_operator(i: LocatedSpan) -> IResult<BinaryOperator> {
  alt((
    op_and, op_or, op_before, op_after, op_eq, op_ne, op_lte, op_lt, op_gte, op_gt, op_match,
    op_imatch,
  ))(i)
}
fn op_and(i: LocatedSpan) -> IResult<BinaryOperator> {
  value(BinaryOperator::And, keyword("and"))(i)
}
fn op_or(i: LocatedSpan) -> IResult<BinaryOperator> {
  value(BinaryOperator::Or, keyword("or"))(i)
}
fn op_before(i: LocatedSpan) -> IResult<BinaryOperator> {
  value(BinaryOperator::Lte, keyword("before"))(i)
}
fn op_after(i: LocatedSpan) -> IResult<BinaryOperator> {
  value(BinaryOperator::Gte, keyword("after"))(i)
}
fn op_eq(i: LocatedSpan) -> IResult<BinaryOperator> {
  value(BinaryOperator::Eq, tag("=="))(i)
}
fn op_ne(i: LocatedSpan) -> IResult<BinaryOperator> {
  value(BinaryOperator::Ne, tag("!="))(i)
}
fn op_lt(i: LocatedSpan) -> IResult<BinaryOperator> {
  value(BinaryOperator::Lt, tag("<"))(i)
}
fn op_lte(i: LocatedSpan) -> IResult<BinaryOperator> {
  value(BinaryOperator::Lte, tag("<="))(i)
}
fn op_gt(i: LocatedSpan) -> IResult<BinaryOperator> {
  value(BinaryOperator::Gt, tag(">"))(i)
}
fn op_gte(i: LocatedSpan) -> IResult<BinaryOperator> {
  value(BinaryOperator::Gte, tag(">="))(i)
}
fn op_match(i: LocatedSpan) -> IResult<BinaryOperator> {
  value(BinaryOperator::Match, keyword("match"))(i)
}
fn op_imatch(i: LocatedSpan) -> IResult<BinaryOperator> {
  value(BinaryOperator::IMatch, keyword("imatch"))(i)
}

#[cfg(test)]
mod tests {
  use crate::{
    ast::{
      BinaryExpression, BinaryOperator, Duration, Expression, RelativeTime, TimeAnchor, Value,
    },
    parser::{
      binary_expr,
      utils::{span, unwrap_span},
    },
  };

  #[test]

  fn test_binary_expr() {
    assert_eq!(
      binary_expr(span(".a.b.c and true")).map(unwrap_span),
      Ok((
        "",
        Expression::BinaryExpression(Box::new(BinaryExpression {
          lhs: Expression::AbsolutePath(vec!["a".to_string(), "b".to_string(), "c".to_string(),]),
          op: BinaryOperator::And,
          rhs: Expression::Value(Value::Bool(true)),
        }))
      ))
    );

    assert_eq!(
      binary_expr(span("true and false or false and true")).map(unwrap_span),
      Ok((
        "",
        Expression::BinaryExpression(Box::new(BinaryExpression {
          lhs: Expression::BinaryExpression(Box::new(BinaryExpression {
            lhs: Expression::Value(Value::Bool(true)),
            op: BinaryOperator::And,
            rhs: Expression::Value(Value::Bool(false)),
          })),
          op: BinaryOperator::Or,
          rhs: Expression::BinaryExpression(Box::new(BinaryExpression {
            lhs: Expression::Value(Value::Bool(false)),
            op: BinaryOperator::And,
            rhs: Expression::Value(Value::Bool(true)),
          })),
        }))
      ))
    );

    assert_eq!(
      binary_expr(span("true and false or false and true == 0 > 3")).map(unwrap_span),
      Ok((
        "",
        Expression::BinaryExpression(Box::new(BinaryExpression {
          lhs: Expression::BinaryExpression(Box::new(BinaryExpression {
            lhs: Expression::Value(Value::Bool(true)),
            op: BinaryOperator::And,
            rhs: Expression::Value(Value::Bool(false)),
          })),
          op: BinaryOperator::Or,
          rhs: Expression::BinaryExpression(Box::new(BinaryExpression {
            lhs: Expression::Value(Value::Bool(false)),
            op: BinaryOperator::And,
            rhs: Expression::BinaryExpression(Box::new(BinaryExpression {
              lhs: Expression::Value(Value::Bool(true)),
              op: BinaryOperator::Eq,
              rhs: Expression::BinaryExpression(Box::new(BinaryExpression {
                lhs: Expression::Value(Value::Int(0)),
                op: BinaryOperator::Gt,
                rhs: Expression::Value(Value::Int(3)),
              })),
            }))
          })),
        }))
      ))
    );

    assert_eq!(
      binary_expr(span(".time after 10 minutes ago")).map(unwrap_span),
      Ok((
        "",
        Expression::BinaryExpression(Box::new(BinaryExpression {
          lhs: Expression::AbsolutePath(vec!["time".to_string()]),
          op: BinaryOperator::Gte,
          rhs: Expression::Value(Value::RelativeTime(RelativeTime::Duration(
            Duration::Minutes(10),
            TimeAnchor::Ago
          ))),
        }))
      ))
    );
  }
}
